#!/bin/sh
set -e -x

apt-get --yes --quiet update
apt-get --yes --quiet install git puppet-common

#
# Fetch puppet configuration from public git repository.
#

mv /etc/puppet /etc/puppet.orig
git clone https://aryansubhash@bitbucket.org/aryansubhash/hello-ec2-puppetboot.git /etc/puppet

#
# Run puppet.
#

puppet apply /etc/puppet/manifests/init.pp